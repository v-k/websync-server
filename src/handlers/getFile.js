const asyncHandler = require('express-async-handler')
const log = require('../logger')
const { verifyPathIsRelative } = require('../utils/validators')
const sendFileContent = require('../utils/sendFileContent')
const sendStatusIfPossible = require('../utils/sendStatusIfPossible')
const sendDirContent = require('../utils/sendDirContent')
const parsePath = require('../utils/parsePath')

module.exports = (app, ctx) => {
  app.get(
    '*',
    asyncHandler(async (req, res, next) => {
      if (!verifyPathIsRelative(req, res)) {
        return
      }

      try {
        const parsedPath = parsePath(req.path, ctx.config)

        if (await parsedPath.isDirectory()) {
          await sendDirContent(res, parsedPath.getPhysicalDirName())
        } else {
          await sendFileContent(res, parsedPath)
        }
      } catch (e) {
        if (e.code === 'ENOENT' || e.message === 'Not found') {
          log.error('File not found', e.path)
          sendStatusIfPossible(res, 404)
        } else {
          log.error('Unexpected error', e)
          sendStatusIfPossible(res, 500)
        }
      } finally {
        next()
      }
    })
  )
}
