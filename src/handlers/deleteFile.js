const path = require('path')
const asyncHandler = require('express-async-handler')
const log = require('../logger')
const { verifyPathIsRelative } = require('../utils/validators')
const parsePath = require('../utils/parsePath')
const { unlink } = require('../utils/fsPromises')
const {
  removeEmptyParentDirs,
  getAllFileVersions,
  getLatestVersionAttributes,
} = require('../utils/fsTools')

module.exports = (app, ctx) => {
  app.delete(
    '*',
    asyncHandler(async (req, res, next) => {
      try {
        if (!verifyPathIsRelative(req, res)) {
          return
        }

        const parsedPath = parsePath(req.path, ctx.config)

        const { lockFlag } = await getLatestVersionAttributes(parsedPath)
        if (lockFlag) {
          res.status(423).send()
          return
        }

        const allVersions = await getAllFileVersions(parsedPath)
        for (let fileVersion of allVersions) {
          await unlink(
            path.join(parsedPath.getPhysicalPathToFile(), fileVersion)
          )
        }
        await removeEmptyParentDirs(parsedPath, ctx.config)

        if (allVersions.length > 0) {
          res.status(200).send()

          ctx.io.emit('update', {
            event: 'delete',
            resource: '/' + parsedPath.getLogicalPath(),
          })
        } else {
          res.status(404).send()
        }
      } catch (e) {
        if (e.code === 'ENOENT' || e.message === 'Not found') {
          res.status(404).send()
        } else {
          log.error('Unexpected error', e)
          res.status(500).send()
        }
      } finally {
        next()
      }
    })
  )
}
