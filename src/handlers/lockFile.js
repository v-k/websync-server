const asyncHandler = require('express-async-handler')
const log = require('../logger')
const { verifyPathIsRelative } = require('../utils/validators')
const parsePath = require('../utils/parsePath')
const { getLatestVersionAttributes } = require('../utils/fsTools')
const { rename } = require('../utils/fsPromises')

module.exports = (app, ctx) => {
  app.lock(
    '*',
    asyncHandler(async (req, res, next) => {
      try {
        if (!verifyPathIsRelative(req, res)) {
          return
        }

        const parsedPath = parsePath(req.path, ctx.config)
        const { timeStamp, lockFlag } = await getLatestVersionAttributes(
          parsedPath
        )

        if (await parsedPath.isDirectory()) {
          log.info('Could not lock a directory', req.path)
          res.status(400).send()
          return
        }

        if (lockFlag) {
          res.status(423).send()
        } else {
          log.info('Locking file', req.path)

          const fullFileNameLocked = parsedPath.formatPhysicalFileName(
            timeStamp,
            true
          )
          const fullFileNameUnlocked = parsedPath.formatPhysicalFileName(
            timeStamp,
            false
          )
          await rename(fullFileNameUnlocked, fullFileNameLocked)

          res.status(200).send()
        }
      } finally {
        next()
      }
    })
  )
}
