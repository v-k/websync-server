const asyncHandler = require('express-async-handler')
const log = require('../logger')
const { verifyPathIsRelative } = require('../utils/validators')
const { mkdir } = require('../utils/fsPromises')
const storeToFile = require('../utils/storeToFile')
const parsePath = require('../utils/parsePath')
const {
  isPathConflicting,
  touchOwningDirs,
  getLatestVersionAttributes,
} = require('../utils/fsTools')

module.exports = (app, ctx) => {
  app.put(
    '*',
    asyncHandler(async (req, res, next) => {
      try {
        if (!verifyPathIsRelative(req, res)) {
          return
        }

        const parsedPath = parsePath(req.path, ctx.config)
        if (!parsedPath.fileName) {
          res.status(400).send()
          return
        }

        if (
          (await isPathConflicting(ctx.config, parsedPath)) ||
          (await parsedPath.isDirectory())
        ) {
          res.status(409).send()
          return
        }

        const pathToFile = parsedPath.getPhysicalPathToFile()

        await mkdir(pathToFile, {
          recursive: true,
        })

        const { fileName, lockFlag } = await getLatestVersionAttributes(
          parsedPath
        )
        if (lockFlag) {
          res.status(423).send()
          return
        }

        const etag = Date.now()
        const newPhysicalFileName =
          parsedPath.getPhysicalDirName() + '.' + String(etag)
        await storeToFile(req, newPhysicalFileName)

        await touchOwningDirs(parsedPath, ctx.config, etag / 1000)

        // TODO: We're not deleting old versions to avoid race conditions.
        // We need a garbage collector.

        res.set('Stored-With-Timestamp', etag)
        if (fileName) {
          res.status(200).send()
        } else {
          res.status(201).send()
        }

        ctx.io.emit('update', {
          event: 'update',
          resource: '/' + parsedPath.getLogicalPath(),
        })
      } catch (e) {
        res.status(500).send()
        log.error(e)
      } finally {
        next()
      }
    })
  )
}
