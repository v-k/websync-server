const asyncHandler = require('express-async-handler')
const log = require('../logger')
const { verifyPathIsRelative } = require('../utils/validators')
const parsePath = require('../utils/parsePath')
const { getLatestVersionAttributes } = require('../utils/fsTools')
const { rename } = require('../utils/fsPromises')

module.exports = (app, ctx) => {
  app.unlock(
    '*',
    asyncHandler(async (req, res, next) => {
      if (!verifyPathIsRelative(req, res)) {
        return
      }

      const parsedPath = parsePath(req.path, ctx.config)
      const { timeStamp, lockFlag } = await getLatestVersionAttributes(
        parsedPath
      )

      if (!lockFlag) {
        log.info('File not locked, could not unlock', req.path)
        res.status(400).send()
      } else {
        log.info('Unlocking file', req.path)

        const fullFileNameLocked = parsedPath.formatPhysicalFileName(
          timeStamp,
          true
        )
        const fullFileNameUnlocked = parsedPath.formatPhysicalFileName(
          timeStamp,
          false
        )
        await rename(fullFileNameLocked, fullFileNameUnlocked)

        res.status(200).send()
      }
      next()
    })
  )
}
