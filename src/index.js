/* eslint-disable no-console */

const http = require('http')
const socketio = require('socket.io')
const express = require('express')
const bodyParser = require('body-parser')

const registerRequestLogger = require('./middlewares/requestLogger')
const registerResponseLogger = require('./middlewares/responseLogger')

const registerGetFile = require('./handlers/getFile')
const registerPutFile = require('./handlers/putFile')
const registerDeleteFile = require('./handlers/deleteFile')
const registerLockFile = require('./handlers/lockFile')
const registerUnlockFile = require('./handlers/unlockFile')

const config = require('../websync.config.json')

const app = express()

const server = http.createServer(app)
const io = socketio(server, {
  path: '/websync.io',
  serveClient: false,
})

io.on('connection', () => {
  console.log('Connected!')
})

app.use(bodyParser.raw())
registerRequestLogger(app)

const context = {
  config,
  io,
}

registerGetFile(app, context)
registerPutFile(app, context)
registerDeleteFile(app, context)
registerLockFile(app, context)
registerUnlockFile(app, context)

registerResponseLogger(app)

server.listen(config.port, config.host, () =>
  console.log(`Websync server is listening on port ${config.port}!`)
)
