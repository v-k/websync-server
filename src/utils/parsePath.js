const path = require('path')
const { stat } = require('./fsPromises')

module.exports = function parsePath(rawPath, config) {
  const parsedPath = rawPath
    .split('/')
    .map(part => decodeURIComponent(part))
    .filter(part => Boolean(part))

  const fileName = parsedPath.pop()

  const fullPath = parsedPath.map(part => ({
    encoded: Buffer.from(part).toString('base64'),
    plain: part,
  }))

  const encodedPath = fullPath.map(path => path.encoded)

  const pathData = {
    encodedPath: encodedPath.join('/'),
    encodedPathSplit: encodedPath,
    fullPath,
    fileName,
    encodedFileName: fileName ? Buffer.from(fileName).toString('base64') : '',
  }

  async function isDirectory() {
    try {
      const dirStat = await stat(getPhysicalDirName())
      return dirStat.isDirectory()
    } catch {
      return false
    }
  }

  function getPhysicalDirName() {
    return path.join(
      config.dataDirectory,
      pathData.encodedPath,
      pathData.encodedFileName
    )
  }

  function formatPhysicalFileName(version, isLocked = false) {
    return path.join(
      config.dataDirectory,
      pathData.encodedPath,
      pathData.encodedFileName + '.' + version + (isLocked ? '.LOCK' : '')
    )
  }

  function getPhysicalPathToFile() {
    return path.join(config.dataDirectory, pathData.encodedPath)
  }

  function getLogicalPath() {
    return path.join(...parsedPath, fileName)
  }

  return {
    ...pathData,
    isDirectory,
    getPhysicalDirName,
    getPhysicalPathToFile,
    getLogicalPath,
    formatPhysicalFileName,
  }
}
