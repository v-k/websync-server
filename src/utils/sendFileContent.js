const fs = require('fs')
const path = require('path')
const mime = require('mime')
const { readdir } = require('./fsPromises')
const { getFileNameAndTimestamp } = require('./fileAttributes')

module.exports = function sendFileContent(res, parsedPath) {
  return new Promise((resolve, reject) => {
    readdir(parsedPath.getPhysicalPathToFile())
      .then(dirItems => {
        const lastFileVersion = dirItems
          .filter(item => item.startsWith(parsedPath.encodedFileName))
          .sort()
          .pop()

        if (!lastFileVersion) {
          reject(new Error('Not found'))
        }

        const fileStream = fs.createReadStream(
          path.join(parsedPath.getPhysicalPathToFile(), lastFileVersion)
        )

        const mimeType = mime.getType(parsedPath.fileName)
        res.set('Content-Type', mimeType || 'application/octet-stream')

        const { timeStamp } = getFileNameAndTimestamp(lastFileVersion)
        res.set('Last-Modified', new Date(Number(timeStamp)).toUTCString())
        res.set('ETag', timeStamp)

        fileStream.pipe(res)

        fileStream.on('end', () => {
          resolve()
        })

        fileStream.on('error', e => {
          reject(e)
        })
      })
      .catch(e => reject(e))
  })
}
