const path = require('path')
const log = require('../logger')
const { unlink, readdir, rmdir, utimes } = require('./fsPromises')
const { getFileNameAndTimestamp } = require('./fileAttributes')

async function touchOwningDirs(parsedPath, config, newTimeStamp) {
  const pathPieces = [...parsedPath.encodedPathSplit]
  const promises = []

  while (pathPieces.length > 0) {
    promises.push(
      utimes(
        path.join(config.dataDirectory, ...pathPieces),
        newTimeStamp,
        newTimeStamp
      )
    )
    pathPieces.pop()
  }

  await Promise.all(promises)
}

async function removeEmptyParentDirs(parsedPath, config) {
  const pathPieces = [...parsedPath.encodedPathSplit]

  while (pathPieces.length > 0) {
    const dirPath = path.join(config.dataDirectory, ...pathPieces)
    const items = await readdir(dirPath)
    const itemCount = items.filter(item => item[0] !== '.').length

    if (itemCount === 0) {
      log.info(`Found empty directory ${dirPath}, deleting.`)
      for (let item of items) {
        await unlink(item)
      }
      await rmdir(dirPath)
    } else {
      log.info(`Considered directory ${dirPath}, not deleting as not empty.`)
    }

    pathPieces.pop()
  }
}

async function isPathConflicting(config, parsedPath) {
  let root = config.dataDirectory
  let plainRoot = ''

  for (let { encoded, plain } of parsedPath.fullPath) {
    try {
      const fileVersions = await getAllFileVersionsInternal(root, encoded)
      if (fileVersions.length > 0) {
        log.info(
          'There is a file that already exists with conflicting names.',
          path.join(plainRoot, plain)
        )
        return true
      }
    } catch {
      // doesn't exist, no conflict, skip
    }

    root = path.join(root, encoded)
    plainRoot = path.join(plainRoot, plain)
  }

  return false
}

async function getAllFileVersionsInternal(dir, fileName) {
  const dirItems = await readdir(dir)
  return dirItems.filter(item => item.startsWith(fileName) && item !== fileName)
}

async function getAllFileVersions(parsedPath) {
  return await getAllFileVersionsInternal(
    parsedPath.getPhysicalPathToFile(),
    parsedPath.encodedFileName
  )
}

async function getLatestVersionAttributes(parsedPath) {
  const lastVersion = (await getAllFileVersions(parsedPath)).sort().pop()
  if (!lastVersion) {
    return {}
  }

  const fullFileName = path.join(
    parsedPath.getPhysicalPathToFile(),
    lastVersion
  )
  return getFileNameAndTimestamp(fullFileName)
}

module.exports = {
  touchOwningDirs,
  removeEmptyParentDirs,
  isPathConflicting,
  getAllFileVersions,
  getLatestVersionAttributes,
}
