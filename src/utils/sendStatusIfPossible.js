module.exports = function sendStatusIfPossible(res, status) {
  if (!res.headersSent) {
    res.status(status).send()
  }
}
