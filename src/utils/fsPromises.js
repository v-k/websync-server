const { promisify } = require('util')

module.exports = {
  mkdir: promisify(require('fs').mkdir),
  rmdir: promisify(require('fs').rmdir),
  readdir: promisify(require('fs').readdir),
  readFile: promisify(require('fs').readFile),
  rename: promisify(require('fs').rename),
  stat: promisify(require('fs').stat),
  unlink: promisify(require('fs').unlink),
  utimes: promisify(require('fs').utimes),
  writeFile: promisify(require('fs').writeFile),
}
