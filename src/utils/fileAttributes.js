function getFileNameAndTimestamp(fileName) {
  const splitName = fileName.split('.')

  const lastBit = splitName.pop()
  if (lastBit === 'LOCK') {
    const timeStamp = splitName.pop()
    return { fileName: splitName.join('.'), timeStamp, lockFlag: lastBit }
  } else {
    const timeStamp = lastBit
    return { fileName: splitName.join('.'), timeStamp }
  }
}

module.exports = {
  getFileNameAndTimestamp,
}
