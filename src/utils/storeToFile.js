const fs = require('fs')

module.exports = function storeToFile(req, fullFileName) {
  return new Promise((resolve, reject) => {
    const fileStream = fs.createWriteStream(fullFileName)

    req.pipe(fileStream)

    req.on('end', () => {
      resolve()
    })

    req.on('error', e => {
      reject(e)
    })
  })
}
