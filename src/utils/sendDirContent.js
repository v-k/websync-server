const path = require('path')
const { readdir, stat } = require('./fsPromises')
const { getFileNameAndTimestamp } = require('./fileAttributes')

const TYPE_FILE = 'file'
const TYPE_COLLECTION = 'collection'

function formatFileInfo(name, stats) {
  if (stats.isDirectory()) {
    return {
      type: TYPE_COLLECTION,
      name: Buffer.from(name, 'base64').toString(),
      etag: stats.mtimeMs,
      lastModified: new Date(stats.mtime).toUTCString(),
    }
  } else {
    const { fileName, timeStamp, lockFlag } = getFileNameAndTimestamp(name)
    return {
      type: TYPE_FILE,
      name: Buffer.from(fileName, 'base64').toString(),
      size: stats.size,
      etag: timeStamp,
      locked: Boolean(lockFlag),
      lastModified: new Date(stats.mtime).toUTCString(),
    }
  }
}

function getOnlyLatestVersions(itemStats) {
  const items = {}
  itemStats.forEach(item => {
    const { fileName, timeStamp } = item.stats.isDirectory()
      ? { fileName: item.name, timeStamp: 0 }
      : getFileNameAndTimestamp(item.name)

    if (
      !items[fileName] ||
      (items[fileName] && items[fileName].timeStamp < timeStamp)
    ) {
      items[fileName] = {
        ...item,
        info: formatFileInfo(item.name, item.stats),
      }
    }
  })

  return items
}

module.exports = async function sendDirContent(res, fullDirName) {
  const dirItems = await readdir(fullDirName)
  const dirStats = await stat(fullDirName)

  const itemStats = await Promise.all(
    dirItems
      .filter(item => item[0] !== '.')
      .map(async item => ({
        name: item,
        stats: await stat(path.join(fullDirName, item)),
      }))
  )

  const items = getOnlyLatestVersions(itemStats)

  res.set('Content-Type', 'application/json')
  res.set('Last-Modified', new Date(Number(dirStats.mtimeMs)).toUTCString())
  res.set('ETag', dirStats.mtimeMs)
  res.json({
    content: Object.values(items).map(item => item.info),
  })
}
