const log = require('../logger')

function verifyPathIsRelative(req, res) {
  if (req.path.match(/\.\.\//)) {
    log.info('"../" in path is not allowed')
    res.status(418).send()
    return false
  }

  return true
}

module.exports = {
  verifyPathIsRelative,
}
