const log = require('../logger')

module.exports = app => {
  app.use(function requestLogger(req, res, next) {
    log.info(`Request ${req.ip} [${req.method}] ${req.path}`)
    next()
  })
}
