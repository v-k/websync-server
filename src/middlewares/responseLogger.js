const log = require('../logger')

module.exports = app => {
  app.use((req, res, next) => {
    log.info(
      `Response ${req.ip} [${req.method}] [${res.statusCode}] ${req.path}`
    )
    next()
  })
}
