# To start the process:

1) Copy `websync.config.json.default` to `websync.config.json` and adjust the content.
   Data directory needs to exist and be a valid directory.
2) npm install
3) npm start
